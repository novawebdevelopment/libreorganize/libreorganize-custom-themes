from django.views.generic import TemplateView
from django.urls import path

from core.default_urls import *


urlpatterns += [
    path("", TemplateView.as_view(template_name="home.html"), name="home"),
]